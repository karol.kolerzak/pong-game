import pygame
import random

pygame.font.init()

WIDTH, HEIGHT = 900, 500
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
VEL = 20
BALL_VEL = 8
PLAYER_HEIGHT = 100
PLAYER_WIDTH = 10
RESULT_FONT = pygame.font.SysFont('arial', 30)
WIN_FONT = pygame.font.SysFont('arial', 100)


#######################################################################################################################

class Player():

    def __init__(self, x):
        self.height = PLAYER_HEIGHT
        self.width = PLAYER_WIDTH
        self.rect = pygame.Rect(x, HEIGHT / 2 - PLAYER_HEIGHT / 2, PLAYER_WIDTH, PLAYER_HEIGHT)
        self.points = 0

    def movement(self, key):
        if key[pygame.K_w] and player_a.rect.y - VEL > 0 - 5:
            player_a.rect.y -= VEL

        if key[pygame.K_s] and player_a.rect.y + VEL < HEIGHT - PLAYER_HEIGHT + 5:
            player_a.rect.y += VEL

        if key[pygame.K_PAGEUP] and player_b.rect.y - VEL > 0 - 5:
            player_b.rect.y -= VEL

        if key[pygame.K_PAGEDOWN] and player_b.rect.y + VEL < HEIGHT - PLAYER_HEIGHT + 5:
            player_b.rect.y += VEL


class Ball():

    def __init__(self):
        self.x = WIDTH / 2
        self.y = HEIGHT / 2
        self.color = RED
        self.radius = 10
        self.velocity_x = BALL_VEL
        self.velocity_y = BALL_VEL
        self.shape = pygame.draw.circle(WINDOW, WHITE, (self.x, self.y), self.radius)
        self.step = 1

    def draw_ball(self):
        self.shape = pygame.draw.circle(WINDOW, self.color, (self.x, self.y), self.radius)

    def ball_movement(self):
        self.draw_ball()
        self.check_position()
        self.x += self.velocity_x
        self.y += self.velocity_y

    def check_position(self):
        if player_a.rect.colliderect(self.shape):  # and self.x in range(0, PLAYER_WIDTH):
            self.velocity_x *= -1
            self.step += 1
        elif player_b.rect.colliderect(self.shape):  # and self.x in range (WIDTH - PLAYER_WIDTH, WIDTH):
            self.velocity_x *= -1
            self.step += 1

        if self.x == 0 or self.x < 0:
            player_b.points += 1
            self.x = WIDTH / 2
            self.y = HEIGHT / 2
            self.velocity_x = BALL_VEL
            self.velocity_y = BALL_VEL
            pygame.time.delay(2000)

        elif self.x == WIDTH or self.x > WIDTH:
            player_a.points += 1
            self.x = WIDTH / 2
            self.y = HEIGHT / 2
            self.velocity_x = BALL_VEL
            self.velocity_y = BALL_VEL
            pygame.time.delay(2000)

        if self.y <= 0:
            self.velocity_y *= -1
        elif self.y >= HEIGHT:
            self.velocity_y *= -1

        self.increase_vell()

    def increase_vell(self):
        if self.step % 3 == 0:
            self.velocity_x = int(self.velocity_x * random.uniform(1.1, 1.3))
            self.velocity_y = int(self.velocity_y * random.uniform(1.1, 1.3))
            self.step = 1


def print_window():
    WINDOW.fill((0, 0, 0))
    border = pygame.Rect(WIDTH / 2 - 1, 0, 2, HEIGHT)
    pygame.draw.rect(WINDOW, WHITE, border)

    pygame.draw.rect(WINDOW, WHITE, player_a.rect)
    pygame.draw.rect(WINDOW, WHITE, player_b.rect)

    ball.ball_movement()

    player_a_results_text = RESULT_FONT.render("Points: " + str(player_a.points), 1, WHITE)
    player_b_results_text = RESULT_FONT.render("Points: " + str(player_b.points), 1, WHITE)

    speed = RESULT_FONT.render("Speed: " + str(abs(ball.velocity_y)), 1, BLUE)

    WINDOW.blit(player_a_results_text, (10, 10))
    WINDOW.blit(player_b_results_text, (WIDTH - player_b_results_text.get_width() - 10, 10))
    WINDOW.blit(speed, (WIDTH / 2 + 10, 10))

    check_result()
    pygame.display.update()


def check_result():
    if player_a.points == 2:
        player_a_win_text = WIN_FONT.render("Player A WIN!!!", 1, RED)
        WINDOW.blit(player_a_win_text,
                    (WIDTH / 2 - player_a_win_text.get_width() / 2, HEIGHT / 2 - player_a_win_text.get_height() / 2))
        pygame.display.update()
        pygame.time.delay(2000)
        player_a.points = 0
        player_b.points = 0

    elif player_b.points == 2:
        player_b_win_text = WIN_FONT.render("Player B WIN!!!", 1, RED)
        WINDOW.blit(player_b_win_text,
                    (WIDTH / 2 - player_b_win_text.get_width() / 2, HEIGHT / 2 - player_b_win_text.get_height() / 2))
        pygame.display.update()
        pygame.time.delay(2000)
        player_a.points = 0
        player_b.points = 0


############################################################################################################################################

WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("PONG")

player_a = Player(0)
player_b = Player(WIDTH - PLAYER_WIDTH)

ball = Ball()
ball.draw_ball()

run = True
while run:
    pygame.time.delay(50)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()

    key_pressed = pygame.key.get_pressed()
    player_a.movement(key_pressed)
    player_b.movement(key_pressed)

    print_window()
